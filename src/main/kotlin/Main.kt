abstract class ValidationChain(
    private val next: ValidationChain?
) {

    fun validate(value: String): Boolean {
        val isValid = validationPredicate(value)
        if (isValid.not()) {
            return false
        }
        return validateNext(value)
    }

    protected abstract val validationPredicate: (String) -> Boolean

    private fun validateNext(value: String) = next?.validate(value) ?: true
}

class EmptyValidator(
    next: ValidationChain? = null
) : ValidationChain(next) {

    override val validationPredicate: (String) -> Boolean = String::isNotBlank
}

class SizeValidator(
    private val sizeRange: IntRange,
    next: ValidationChain? = null,
) : ValidationChain(next) {

    override val validationPredicate: (String) -> Boolean = { value ->
        value.length in sizeRange
    }
}

fun main() {
    val password = "       "
    val range = 5..10
//    val isValid = password.isBlank() && password.length in range
    val chain = EmptyValidator(
        SizeValidator(range)
    )
    val isValid = chain.validate(password)
    print(isValid)
}